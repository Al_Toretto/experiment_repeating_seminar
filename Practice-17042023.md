1 - Formulate the problem being solved in the article.

The task of program synthesis. Given a specification, automatically generate a program consistent with this specificaion.

2 - how is the task bengin solved?

using a Seq2Seq machine-translation model, but with some modifications to avoid the problems of Seq2Seq models in generated structured text.

3 - What method is used to solve the problem?

reinforcement learning is performed over the top of the supervised model with an objective that explicitly maximizes the likleyhood og generating semantically correct programs.

Syntax conditioning: syntactically incorrect programs are directly pruned from the output space.

4 - List the main results of the article.
-They that Reinforcement Learning can directly optimize for generating any consistent program and improves performance compared to pure supervised learning.
-They introduced a method for pruning the space of possible programs using a syntax checker and showed that explicit syntax checking helps generate better programs.
-In the absence of a syntax checker, they introduced a model that jointly learns syntax and the production of correct programs. They demonstrate that this model improves performance in instances with limited training data.

---------------------------------------------------
I ran the model code 3 times on colab but the last cell always gets stuck, that's way I will try to anwer the quesions from the paper.

This screenshot was taken three hours after running the cell:
![screen](./image.png)


1 - what data is used?

The kerel program DSL dataset.

2 - what variants of the training model are implemented?

In the paper the following models were implemented:
-MLE
-RL
-RL_beam
-RL_beam_div
-RL_beam_dv

Each model is tried with the full dataset, and with a smaller version of it.

In our experiment the model was executed on 1%, 10%, or 100% of the data

3 - what will the model give out after training?

The accuracy metrics will be shown: generalizaion accuracy & exact match accuracy

4 - how many epochs does it take to train each variant? Justify the answer.

5 - train the models for 1% of the data with suitable hyperparameters

6 - get 10 predictions for different tasks from the test dataset.